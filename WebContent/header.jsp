<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Locadora LocaFácil</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="assets/css/paper-bootstrap.min.css">
		<link src="assets/css/styles.css">
		<link rel="stylesheet" href="assets/fa/css/font-awesome.css">
	</head>
	<body>
	