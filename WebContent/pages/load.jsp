<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp"></jsp:include>
<div class="container">

	<jsp:useBean id="dao" class="br.com.agenda.dao.ContatoSQLDAO"/>
	
	<c:forEach var="contato" items="${dao.load()}">
	
		<div class="box">
		  <article class="media">
		    <div class="media-left">
			    <span class="icon is-large">
				  <i class="fa fa-user"></i>
				</span>
		    </div>
		    <div class="media-content">
		      <div class="content">
		        <p>
		          <strong>${contato.nome}</strong>
		          <small>${contato.email}</small>
		          <small>${contato.ramal}</small>
		          <br>
		          ${contato.setor}
		        </p>
		      </div>
		    </div>
		  </article>
		</div>
		
	</c:forEach>
  
</div>
<jsp:include page="../footer.jsp"></jsp:include>